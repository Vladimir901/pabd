# Предиктивная аналитика больших данных 

Сервер flask запускается из командной строки командой. 
```
python src/predict_app.py 
```
По умолчанию сервис доступен по адресу http://127.0.0.1:5000/  


Kaggle dataset для базового обучения модели можно скачать [здесь.](https://storage.yandexcloud.net/pabd/kaggle.zip)  

Инструкция по установке [docker](https://docs.docker.com/engine/install/ubuntu/) 
Инструкция по установке [gitlab-runner](https://docs.gitlab.com/runner/install/linux-manually.html) 
